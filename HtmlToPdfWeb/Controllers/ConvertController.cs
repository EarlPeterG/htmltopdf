﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace HtmlToPdfWeb.Controllers
{
    public class ConvertController : Controller {
        #region "Local functions"
		private void LogToFile(string filename, string logMsg) {
			System.IO.File.AppendAllText(filename, "[" + DateTime.Now.ToString() + "] " + logMsg + Environment.NewLine);

			// 10 mb
			if (new FileInfo(filename).Length > 10485760) {
				System.IO.File.Move(filename, Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename) + DateTime.Now.ToString(" dd-MM-yyyy ") + ".log"));
			}
		}
        private bool CheckURLValid(string source) {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }
        private string GetUriDomain(string uri) {
            Uri myUri = new Uri(uri);
            return myUri.Host;
        }
        private string StripInvalidFileNameChars(string filename, string replacement = "") {
            string fileNameCleanerExpression = "[" + string.Join("", Array.ConvertAll(Path.GetInvalidFileNameChars(), x => Regex.Escape(x.ToString()))) + "]";
            Regex fileNameCleaner = new Regex(fileNameCleanerExpression, RegexOptions.Compiled);

            return fileNameCleaner.Replace(filename, replacement);
        }
        private string GetCurrentRequestId() {
            string id = HttpContext.Timestamp.ToString("dd-MM-yyyy-HH-mm-ss-fffffff") + ":" + Process.GetCurrentProcess().Id.ToString() + ":" + Thread.CurrentThread.ManagedThreadId.ToString();

            var hash = new SHA1CryptoServiceProvider();

            byte[] hashData = hash.ComputeHash(Encoding.Default.GetBytes(id));

            StringBuilder returnValue = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++) {
                returnValue.Append(hashData[i].ToString("x2"));
            }

            return returnValue.ToString();
        }
        private string[] splitMargins(string margin) {
            string[] margins = margin.Split(' ');

            if (margins.Length == 4)
                return margins;
            else if (margins.Length == 2)
                return new string[] { margins[0], margins[1], margins[0], margins[1] };
            else
                return new string[] { margin, margin, margin, margin };
        }
        public static bool IsHtmlValid(string html) {
            // False = SmallerThan
            // True = GreaterThan

            bool expected = false; // Must start with < [Smaller Than]
            for (int i = 0; i < html.Length; i++) {
                // Letter.
                char letter = html[i];

                // Common case.
                if (letter != '>' &&
                    letter != '<') {
                    continue;
                }

                // False = SmallerThan [<]
                // True = GreaterThan [>]
                bool found = letter == '>';

                // If we found what we expected, expect the opposite next.
                if (found == expected) {
                    expected = !expected;
                }
                else {
                    // Disallow.
                    return false;
                }
            }

            // Return true if expected is false [we expect < SmallerThan]
            return !expected;
        }

        //Int32 sleeptime = 10000; // 10 seconds
        //Int32 sleeptime = 60000; // 1 minute
        Int32 sleeptime = 600000; // 10 minutes
        //Int32 sleeptime = 1800000; // 30 minutes
        //Int32 sleeptime = 3600000; // 1 hour
        #endregion

        //
        // GET: /Convert/
        [AllowAnonymous]
        public ActionResult Index(string url, string pagesize = "A4", string margin = "2.645833mm 2.645833mm 2.645833mm 2.645833mm") {
            // verify if url is on whitelisted domains
            string[] AllowedDomains = System.IO.File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "domains.txt"));
            string UrlDomain = GetUriDomain(url);
            bool allowed = false;
            foreach (string domain in AllowedDomains) {
                if(!string.IsNullOrWhiteSpace(domain) && UrlDomain.Contains(domain)){
                    allowed = true;
                    break;
                }
            }
            if(!allowed){
                return new HttpStatusCodeResult(403, "Not Allowed");
            }

            string[] margins = splitMargins(margin);
            if (!string.IsNullOrWhiteSpace(url) && CheckURLValid(url) && !string.IsNullOrWhiteSpace(pagesize) && !pagesize.Contains(' ') && !string.IsNullOrWhiteSpace(margin) && margins.Length == 4) {
                // dedicate a request id
                string CurrentRequestId = GetCurrentRequestId();

                // create the filename
                string CurrentFolder = Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), CurrentRequestId);
                if (!CurrentFolder.EndsWith(@"\")) CurrentFolder += @"\";

                string CurrentFile = StripInvalidFileNameChars(UrlDomain + ".pdf");

                if (!Directory.Exists(CurrentFolder)) {
                    Directory.CreateDirectory(CurrentFolder);
                }

                if (System.IO.File.Exists(CurrentFolder + CurrentFile)) {
                    return new HttpStatusCodeResult(409, "Conflict");
                }

                // Process path
                var CurrentProcess = new Process();
                CurrentProcess.StartInfo.FileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"bin\wkhtmltopdf\bin\wkhtmltopdf.exe");
                CurrentProcess.StartInfo.Arguments = String.Format("--page-size {0} --margin-bottom {1} --margin-left {2} --margin-right {3} --margin-top {4} {5} \"{6}\"", pagesize, margins[0], margins[1], margins[2], margins[3], url, (CurrentFolder + CurrentFile).Replace("\\", "\\\\"));
                CurrentProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                CurrentProcess.StartInfo.RedirectStandardOutput = true;
                CurrentProcess.StartInfo.UseShellExecute = false;
                CurrentProcess.Start();

                // delete the temporary folder
                new Thread(() => {
                    Thread.Sleep(sleeptime);

                    try {
                        Directory.Delete(CurrentFolder, true);
                    }
					catch (DirectoryNotFoundException){
						// ignore this kind of error
					}
					catch {
						// retry again for the last time
						try {
							Thread.Sleep(sleeptime);
							Directory.Delete(CurrentFolder, true);
                        }
						catch (DirectoryNotFoundException) {
							// ignore this kind of error
						}
						catch (Exception ex) {
							// log error
							LogToFile(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                        }
                    }
                }).Start();

                CurrentProcess.WaitForExit();

                if (!(CurrentProcess.ExitCode == 0 || CurrentProcess.ExitCode == 1) || !System.IO.File.Exists(CurrentFolder + CurrentFile)) {
                    System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), CurrentProcess.StandardOutput.ReadToEnd() + Environment.NewLine);
                    return new HttpStatusCodeResult(500, "Internal Server Error");
                }

				// log conversion
				LogToFile(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "Log.log"), url);

				// redirect to download
				return RedirectToAction("Download", "Convert", new { key = CurrentRequestId + @"\" + CurrentFile });
            }

            // Invalid request
            else {
                return new HttpStatusCodeResult(400, "Bad Request");
            }
        }

        //
        // POST: /Convert/FromHtml
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult FromHtml(string html, string filename = "", string pagesize = "A4", string margin = "2.645833mm 2.645833mm 2.645833mm 2.645833mm") { 
            // TODO: verify post referrer if domain is on whitelist

            filename = StripInvalidFileNameChars(filename, "_");
            string[] margins = splitMargins(margin);
            if (!string.IsNullOrWhiteSpace(filename) && !string.IsNullOrWhiteSpace(html) && IsHtmlValid(html) && !string.IsNullOrWhiteSpace(pagesize) && !pagesize.Contains(' ') && !string.IsNullOrWhiteSpace(margin) && margins.Length == 4) {
                // dedicate a request id
                string CurrentRequestId = GetCurrentRequestId();

                // create the filename
                string CurrentFolder = Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), CurrentRequestId);
                if (!CurrentFolder.EndsWith(@"\")) CurrentFolder += @"\";

                if (System.IO.Directory.Exists(CurrentFolder)) {
                    return new HttpStatusCodeResult(409, "Conflict");
                }
                else {
                    Directory.CreateDirectory(CurrentFolder);
                }

                string CurrentFile = filename == "" ? DateTime.Now.ToString("M-d-yyyy-HH-mm") : filename;
                System.IO.File.WriteAllText(CurrentFolder + CurrentFile + ".html", html);

                // Process path
                var CurrentProcess = new Process();
                CurrentProcess.StartInfo.FileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"bin\wkhtmltopdf\bin\wkhtmltopdf.exe");
                CurrentProcess.StartInfo.Arguments = String.Format("--page-size {0} --margin-bottom {1} --margin-left {2} --margin-right {3} --margin-top {4} \"{5}\" \"{6}\"", pagesize, margins[0], margins[1], margins[2], margins[3], (CurrentFolder + CurrentFile + ".html").Replace("\\", "\\\\"), (CurrentFolder + CurrentFile + ".pdf").Replace("\\", "\\\\"));
                CurrentProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                CurrentProcess.StartInfo.RedirectStandardOutput = true;
                CurrentProcess.StartInfo.UseShellExecute = false;
                CurrentProcess.Start();

                // delete the temporary folder
                new Thread(() => {
                    Thread.Sleep(sleeptime);

                    try {
                        Thread.Sleep(sleeptime);
                        Directory.Delete(CurrentFolder, true);
                    }
                    catch {
                        // retry again for the last time
                        try {
                            Thread.Sleep(sleeptime);
                            Directory.Delete(CurrentFolder, true);
                        }
                        catch (Exception ex1) {
                            // log error
                            System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), ex1.Message + Environment.NewLine);
                            System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), ex1.InnerException + Environment.NewLine);
                            System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), ex1.StackTrace + Environment.NewLine);
                        }
                    }
                }).Start();

                CurrentProcess.WaitForExit();

                if (!(CurrentProcess.ExitCode == 0 || CurrentProcess.ExitCode == 1) || !System.IO.File.Exists(CurrentFolder + CurrentFile + ".pdf")) {
                    System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "ErrorLog.log"), CurrentProcess.StandardOutput.ReadToEnd() + Environment.NewLine);
                    return new HttpStatusCodeResult(500, "Internal Server Error");
                }

                // redirect to download
                return RedirectToAction("Download", "Convert", new { key = CurrentRequestId + @"\" + CurrentFile + ".pdf" });
            }

            // Invalid request
            else {
                return new HttpStatusCodeResult(400, "Bad Request");
            }
        }

        public ActionResult Download(string key) {
            string CurrentFilePath = Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), key);

            if (System.IO.File.Exists(CurrentFilePath)) {
                // return the file
                return File(CurrentFilePath, "application/pdf", System.IO.Path.GetFileName(CurrentFilePath));
            }
            else {
                return new HttpStatusCodeResult(410, "Expired link");
            }
        }

    }
}
